FROM docker.io/node:16-alpine
MAINTAINER pikdum <pikdum@kuudere.moe>
WORKDIR /app/
COPY *.json /app/
RUN npm i --force
COPY . .
ARG CUSTOM_API_BASE=https://kuudere.moe/api
ENV API_BASE $CUSTOM_API_BASE
RUN VERSION=`date +%s` npm run build -- --spa

FROM docker.io/nginx:alpine
MAINTAINER pikdum <pikdum@kuudere.moe>
WORKDIR /app/
RUN apk add --no-cache curl
COPY --from=0 /app/dist/ .
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
HEALTHCHECK CMD curl --fail http://localhost:80 || exit 1
