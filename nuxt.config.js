module.exports = {
    /*
    ** Headers of the page
    */
    head: {
        title: '／(･ × ･)＼',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'kuu2-frontend' }
        ],
        script: [
            { src: 'https://code.jquery.com/jquery-3.6.0.min.js' },
            { src: '/snowflake.js' },
        ],
    },
    /*
    ** Customize the progress bar color
    */
    loading: { color: 'gray' },
    /*
    ** Build configuration
    */
    env: {
        API_BASE: process.env.API_BASE,
        VERSION: process.env.VERSION
    },
    modules: [
        '@nuxtjs/axios'
    ],
    axios: {
        // proxyHeaders: false
    },
    router: {
        scrollBehavior(to, from, savedPosition) {
            if (savedPosition) {
                return savedPosition;
            } else {
                let position = {};
                if (to.matched.length < 2) {
                    position = { x: 0, y: 0 };
                } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
                    position = { x: 0, y: 0 };
                }
                if (to.hash) {
                    position = { selector: to.hash };
                }
                return position;
            }
        }
    },
    render: {
        static: {
            maxAge: 1000 * 60 * 60 * 24 * 7
        }
    }
};
